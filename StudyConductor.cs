using System;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Text;
using System.Collections.Generic;

//TODO: 新規作成したタスクが、作成された日以前に現れないようにする
//TODO: Overdueのタスクをマークしたあと、それを取り消すとOverdueではなくなる問題を解消
//TODO: データの切り替えを可能にする
class Program
{
	static void Main(string[] args)
	{
        new CommandManager();
        return;
	}
}

public class CommandManager
{
    private ConfigManager ConfigManagerInstance;
    private RecordManager RecordManagerInstance;
    private List<Record> TodaysTaskListCache;
    private List<Record> MarkedTaskList;

    public CommandManager()
    {
        ConfigManagerInstance = new ConfigManager();
        RecordManagerInstance = new RecordManager(ConfigManagerInstance.RecordFilePath);
        TodaysTaskListCache = RecordManagerInstance.PickUpNextAppearanceRecord(ConfigManagerInstance.ManipulatingDay, true);
        MarkedTaskList = RecordManagerInstance.PickUpLastMarkedRecord(ConfigManagerInstance.ManipulatingDay, false);

        bool canApplicationBeTerminated = false;
        //TODO: インデントを揃えるようにする
        while(!canApplicationBeTerminated){
            Console.WriteLine("---------------------------------------------");
            if(ConfigManagerInstance.ManipulatingDay == DateTime.Today){
                Console.WriteLine(ConfigManagerInstance.ManipulatingDay.ToString("yyyy年MM月dd日(本日)"));
            }else{
                int dayDiff = (ConfigManagerInstance.ManipulatingDay - DateTime.Today).Days;
                string msg = dayDiff.ToString("+#;-#;");
                Console.WriteLine(ConfigManagerInstance.ManipulatingDay.ToString("yyyy年MM月dd日(" + msg + "日)"));
            }
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("<未実行タスク>");
            int index = 0;
            if(TodaysTaskListCache.Count > 0){
                foreach(Record oneRec in TodaysTaskListCache){
                    Console.Write("(" + index +  ")");

                    string msg = "       : ";
                    int offset = (ConfigManagerInstance.ManipulatingDay - oneRec.NextAppearanceDay).Days;
                    bool isOverDue = false;
                    if(offset != 0){
                        msg = "[OVD" + offset.ToString("+#;-#;") + "]: ";
                        isOverDue = true;
                    }
                    if(oneRec.CreatedDay == ConfigManagerInstance.ManipulatingDay) msg = "[NEW]  : ";

                    if(isOverDue){
                        WriteLineWithErrorColor(msg + oneRec.Content,1);
                    }else{
                        Console.WriteLine(msg + oneRec.Content);
                    }
                    index++;
                }
            }else{
                Console.WriteLine("未実行のタスクはありません");
            }
            Console.WriteLine();

            if(MarkedTaskList.Count > 0){
                Console.WriteLine("---------------------------------------------");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("<実行済みタスク>");
                foreach(Record oneRec in MarkedTaskList){
                    Console.WriteLine("("+ index + ")[DONE] : " + oneRec.Content);
                    index++;
                }
                Console.ResetColor();
                Console.WriteLine();
            }

            Console.WriteLine("<コマンド>");
            Console.WriteLine("(M):マーク, (C):新規作成, (R):改名, (D):削除, (T):時間移動, (Q):終了");
            Console.WriteLine("---------------------------------------------");

            Console.Write(">> ");
            string input = Console.ReadLine();
            switch (input)
            {
                case "M": case "m"://(M):実行済みにする
                    MarkTask();
                    break;

                case "C": case "c"://(C):新規作成
                    CreateTask();
                    break;

                case "R": case "r"://(R):改名
                    RenameTask();
                    break;

                case "D": case "d"://(D):削除
                    DeleteTask();
                    break;

                case "T": case "t"://(T):時間移動
                    MoveTime();
                    break;

                case "Q": case "q"://(Q):終了
                    canApplicationBeTerminated = true;
                    break;

                default:
                    WriteLineWithWarningColor("指定されたアルファベットを入力してください");
                    break;
            }
            Console.WriteLine();

        }
        Console.WriteLine("アプリケーションを終了します");
        return;
    }

    //TODO: MarkTask, DeleteTask, RenameTaskに共通する部分をメソッドへ抽出する
    private void MarkTask()
    {
        while(true){
            if(TodaysTaskListCache.Count == 0 && MarkedTaskList.Count == 0 && TodaysTaskListCache.Count == 0){
                WriteLineWithWarningColor("指定できるタスクが存在しないので、タスクのマーキングをキャンセルします");
                return;
            }

            Console.WriteLine("マークするタスクの番号を入力してください(Qでキャンセル)");
            Console.Write(">> ");
            string input = Console.ReadLine();
            if(CheckWhetherQuitCommand(input)){ Console.WriteLine("タスクのマーキングをキャンセルしました"); return; }

            int numCommand = -1;
            bool isNumber = int.TryParse(input, out numCommand);
            if(isNumber && numCommand >= 0){//(数字): 実行済みにする
                Record designatedRec;
                if(numCommand < TodaysTaskListCache.Count){
                    designatedRec = TodaysTaskListCache[numCommand];
                    int id = designatedRec.TaskID;
                    bool wasCreatedToday = (designatedRec.CreatedDay == ConfigManagerInstance.ManipulatingDay);
                    int lastRLevel = designatedRec.RemembranceLevel;

                    bool isUpdated = false;
                    if(wasCreatedToday){
                        isUpdated = true;
                        designatedRec.RemembranceLevel = 1;
                    }else{
                        Console.WriteLine("どれくらい覚えていましたか");
                        Console.WriteLine("(1):80%以上, (2):40~80%, (3)40%以下");
                        Console.Write(">> ");
                        string input2 = Console.ReadLine();
                        
                        switch(input2){
                            case "1"://Good:レベル1増やす
                                isUpdated = true;
                                designatedRec.RemembranceLevel++;
                                break;
                            case "2"://Fair:レベル不変
                                isUpdated = true;
                                break;
                            case "3"://Bad:レベル2減らす
                                isUpdated = true;
                                designatedRec.RemembranceLevel -= 2;
                                if(designatedRec.RemembranceLevel < 1)
                                    designatedRec.RemembranceLevel = 1;
                                designatedRec.CumulativeMistakes++;
                                break;                            
                        }
                    }

                    if(isUpdated){
                        designatedRec.LastMarkedDay = ConfigManagerInstance.ManipulatingDay;
                        designatedRec.LastRemembranceLevel = lastRLevel;
                        designatedRec.NextAppearanceDay = RecordManagerInstance.ComputeNextAppearanceDay(designatedRec.RemembranceLevel, ConfigManagerInstance.ManipulatingDay);                        
                        RecordManagerInstance.UpdateRecord(designatedRec);

                        Console.WriteLine("「" + designatedRec.Content + "」を実行済みにしました");
                        SaveAndReflesh();
                    } 

                    return;

                }else if(numCommand >= TodaysTaskListCache.Count && numCommand < MarkedTaskList.Count + TodaysTaskListCache.Count){
                    designatedRec = MarkedTaskList[numCommand - TodaysTaskListCache.Count];
                    designatedRec.RemembranceLevel = designatedRec.LastRemembranceLevel;
                    designatedRec.LastMarkedDay = ConfigManagerInstance.ManipulatingDay + new TimeSpan(-1, 0, 0, 0);
                    designatedRec.NextAppearanceDay = ConfigManagerInstance.ManipulatingDay;

                    Console.WriteLine("「" + designatedRec.Content + "」を未実行に戻しました");
                    SaveAndReflesh();
                    return;
                }else{
                    WriteLineWithWarningColor("0以上"+(MarkedTaskList.Count + TodaysTaskListCache.Count - 1).ToString() + "以下の数値を入力してください");
                }
            }else{
                WriteLineWithWarningColor("0以上"+(MarkedTaskList.Count + TodaysTaskListCache.Count - 1).ToString() + "以下の数値を入力してください");
            }
        }
    }

    private void CreateTask()
    {
        while(true){
            Console.WriteLine("新しく作るタスクの内容を入力してください(Qでキャンセル)");
            Console.Write(">> ");
            string input = Console.ReadLine();
            if(CheckWhetherQuitCommand(input)){ Console.WriteLine("タスクの作成をキャンセルしました"); return; }

            if(String.IsNullOrEmpty(input)){
                WriteLineWithWarningColor("何かを入力してください");
            }else{
                RecordManagerInstance.CreateRecord(input, ConfigManagerInstance.ManipulatingDay);
                Console.WriteLine("タスク名: " + input + "が作成されました");

                SaveAndReflesh();
                return;
            }
        }
    }
    private void DeleteTask()//TODO: 削除の際、確認メッセージを出すようにする
    {
        while(true){
            if(TodaysTaskListCache.Count == 0 && MarkedTaskList.Count == 0 && TodaysTaskListCache.Count == 0){
                WriteLineWithWarningColor("指定できるタスクが存在しないので、タスクの削除をキャンセルします");
                return;
            }

            Console.WriteLine("削除するタスクの番号を入力してください(Qでキャンセル)");
            Console.WriteLine("この操作は取り消せないので、慎重に行ってください");
            Console.Write(">> ");
            string input = Console.ReadLine();
            if(CheckWhetherQuitCommand(input)){ Console.WriteLine("タスクの削除をキャンセルしました"); return; }

            int numCommand = -1;
            bool isNumber = int.TryParse(input, out numCommand);
            if(isNumber && numCommand >= 0){
                if(numCommand < TodaysTaskListCache.Count){
                    Record designatedRec = TodaysTaskListCache[numCommand];
                    RecordManagerInstance.DeleteRecord(designatedRec);
                    Console.WriteLine("「" + designatedRec.Content + "」を削除しました");
                    SaveAndReflesh();
                    return;
                }else if(numCommand >= TodaysTaskListCache.Count && numCommand < MarkedTaskList.Count + TodaysTaskListCache.Count){
                    Record designatedRec = MarkedTaskList[numCommand - TodaysTaskListCache.Count];
                    RecordManagerInstance.DeleteRecord(designatedRec);
                    Console.WriteLine("「" + designatedRec.Content + "」を削除しました");
                    SaveAndReflesh();
                    return;

                }else{
                    WriteLineWithWarningColor("0以上"+(MarkedTaskList.Count + TodaysTaskListCache.Count - 1).ToString() + "以下の数値を入力してください");
                }

            }else{
                WriteLineWithWarningColor("0以上"+(MarkedTaskList.Count + TodaysTaskListCache.Count - 1).ToString() + "以下の数値を入力してください");
            }
        }
    }
    private void RenameTask()
    {
        while(true){
            if(TodaysTaskListCache.Count == 0 && MarkedTaskList.Count == 0 && TodaysTaskListCache.Count == 0){
                Console.WriteLine("指定できるタスクが存在しないので、タスクの改名をキャンセルします");
                return;
            }

            Console.WriteLine("改名するタスクの番号を入力してください(Qでキャンセル)");
            Console.Write(">> ");
            string input = Console.ReadLine();
            if(CheckWhetherQuitCommand(input)){ Console.WriteLine("タスクの改名をキャンセルしました"); return; }

            int numCommand = -1;
            bool isNumber = int.TryParse(input, out numCommand);
            if(isNumber && numCommand >= 0){
                if(numCommand < TodaysTaskListCache.Count){
                    Record designatedRec = TodaysTaskListCache[numCommand];
                    string beforeContent = designatedRec.Content;

                    while(true){
                        Console.WriteLine("新しいタスクの内容を入力してください(Qでキャンセル)");
                        Console.Write(">> ");
                        string taskNameInput = Console.ReadLine();
                        if(CheckWhetherQuitCommand(taskNameInput)) break;

                        if(String.IsNullOrEmpty(taskNameInput)){
                            WriteLineWithWarningColor("何か入力してください");

                        }else{
                            designatedRec.Content = taskNameInput;
                            RecordManagerInstance.UpdateRecord(designatedRec);
                            Console.WriteLine("「" + beforeContent + "」を、");
                            Console.WriteLine("「" + taskNameInput + "」へ改名しました");
                            SaveAndReflesh();
                            break;
                        }
                    }
                    return;
                }else if(numCommand >= TodaysTaskListCache.Count && numCommand < MarkedTaskList.Count + TodaysTaskListCache.Count){
                    Record designatedRec = MarkedTaskList[numCommand - TodaysTaskListCache.Count];
                    string beforeContent = designatedRec.Content;

                    while(true){
                        Console.WriteLine("新しいタスクの内容を入力してください(Qでキャンセル)");
                        Console.Write(">> ");
                        string taskNameInput = Console.ReadLine();
                        if(CheckWhetherQuitCommand(taskNameInput)) break;

                        if(String.IsNullOrEmpty(taskNameInput)){
                            WriteLineWithWarningColor("何か入力してください");

                        }else{
                            designatedRec.Content = taskNameInput;
                            RecordManagerInstance.UpdateRecord(designatedRec);
                            Console.WriteLine("「" + beforeContent + "」を、");
                            Console.WriteLine("「" + taskNameInput + "」へ改名しました");
                            SaveAndReflesh();
                            break;
                        }
                    }
                    return;

                }else{
                    WriteLineWithWarningColor("0以上"+(MarkedTaskList.Count + TodaysTaskListCache.Count - 1).ToString() + "以下の数値を入力してください");
                }
            }else{
                WriteLineWithWarningColor("0以上"+(MarkedTaskList.Count + TodaysTaskListCache.Count - 1).ToString() + "以下の数値を入力してください");
            }
        }
    }
    private void MoveTime()
    {
        bool canMoveTime = false;
        while(true){
            Console.WriteLine("移動したい時間を'yyyy/MM/dd'の形式で入力するか、'±d'の形式で入力してください(qでキャンセル)");
            Console.Write(">> ");
            string input = Console.ReadLine();
            if(CheckWhetherQuitCommand(input)){ Console.WriteLine("日付の移動をキャンセルしました"); return; }

            DateTime destinationDay = DateTime.Today;
            bool isDateTime = DateTime.TryParse(input, out destinationDay);
            if(isDateTime){
                canMoveTime = true;

            }else{
                int shiftingDay = 0;
                bool isValidNumber = int.TryParse(input, out shiftingDay);
                if(isValidNumber){
                    if(shiftingDay == 0){
                        WriteLineWithWarningColor("日付をずらす場合、1以上もしくは-1以下の数字を入力してください");

                    }else{
                        canMoveTime = true;
                        destinationDay = ConfigManagerInstance.ManipulatingDay + new TimeSpan(shiftingDay, 0, 0, 0);
                        Console.Write(shiftingDay + "日ずらし、");
                    }
                }else{
                    WriteLineWithWarningColor("日付の移動に失敗しました。");
                    Console.WriteLine();
                }
            }

            if(canMoveTime){
                Console.WriteLine(destinationDay.ToString("yyyy/MM/ddへと日付を変更しました"));
                ConfigManagerInstance.ManipulatingDay = destinationDay;
                SaveAndReflesh();
                return;
            }
        }
    }
    private bool CheckWhetherQuitCommand(string command)
    {
        return (command == "Q" || command == "q") ? true : false;
    }

    //TODO: デフォルト色が強調色と同じ場合、変更するようにする
    private void WriteLineWithWarningColor(string msg, int returningLines = 2){
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(msg);
        for(int i=0; i<returningLines; i++)
            Console.WriteLine();
        Console.ResetColor();
    }
    private void WriteLineWithErrorColor(string msg, int returningLines = 2){
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write(msg);
        for(int i=0; i<returningLines; i++)
            Console.WriteLine();
        Console.ResetColor();
    }

    private void SaveAndReflesh()
    {
        MarkedTaskList = RecordManagerInstance.PickUpLastMarkedRecord(ConfigManagerInstance.ManipulatingDay, false);
        TodaysTaskListCache = RecordManagerInstance.PickUpNextAppearanceRecord(ConfigManagerInstance.ManipulatingDay, true);
        RecordManagerInstance.SaveRecordsToFile();
    }
}

public class RecordManager
{
    private List<Record> RecordList = new List<Record>();
    private string FilePath = "debug.csv";
    private int MaxID = -1;

    public RecordManager(string filePath)
    {
        if(filePath != null){
            FilePath = filePath;
            if(FilePath == "debug.csv"){
                Console.WriteLine();
                Console.WriteLine("デバッグ用のデータを利用します");
                Console.WriteLine();
            }else{
                Console.WriteLine(FilePath + "を読み込みました");
            }
            LoadRecordsFromFile();
        }
    }

    public void LoadRecordsFromFile()
    {
        try{
        using(StreamReader sr = new StreamReader(
        (System.IO.Stream)File.OpenRead(FilePath), Encoding.GetEncoding("UTF-8"))
            ){
                //TODO: try catchを使って書く
                //TODO: 改行を無視するようにする
                string oneLine = "";
                int readingCount = 0;
                List<string> notReadableLine = new List<string>(); 
                while ((oneLine = sr.ReadLine()) != null) {
                    string[] data = oneLine.Split(',');
                    if(data.Length < 6){
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Line");
                        sb.Append(readingCount);
                        sb.Append(": ");
                        sb.Append(oneLine);
                        notReadableLine.Add(sb.ToString());
                        continue;
                    }
                    var rec = new Record("", -1, DateTime.Today);
                    bool isAddable = rec.SetRecord(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
                    if(isAddable){
                        RecordList.Add(rec);
                    }else{
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Line");
                        sb.Append(readingCount);
                        sb.Append(": ");
                        sb.Append(oneLine);
                        notReadableLine.Add(sb.ToString());
                    }


                    int tempID = -1;
                    if(int.TryParse(data[0], out tempID))
                    {
                        if(tempID > MaxID)
                            MaxID = tempID;
                    }
                    readingCount++;
                }
                if(notReadableLine.Count > 0)
                {
                    Console.WriteLine(notReadableLine.Count + "件のデータを読み込むことができませんでした");
                    foreach(string line in notReadableLine)
                    {
                        Console.WriteLine(line);
                    }
                    Console.WriteLine();
                }
            }
        }catch(Exception e){
            Console.WriteLine(e.Message);
        }
    }

    public void SaveRecordsToFile()
    {
        StringBuilder sb = new StringBuilder();
        foreach (Record rec  in RecordList){
            sb.Append(rec.CSV());
            sb.AppendLine();
        }
        File.WriteAllText(FilePath, sb.ToString(), System.Text.Encoding.GetEncoding("UTF-8"));
    }

    public void CreateRecord(string content, DateTime day){
        MaxID++;
        var rec = new Record(content, MaxID, day);
        RecordList.Add(rec);
    }

    //TODO: LINQで書き換える
    public void UpdateRecord(Record rec){
        int id = rec.TaskID;
        int len = RecordList.Count;
        for(int i=0; i<len; i++){
            if(RecordList[i].TaskID == id){
                RecordList[i] = rec;
                break;
            }
        }
    }
    public void DeleteRecord(Record rec){
        RecordList.Remove(rec);
    }

    public DateTime ComputeNextAppearanceDay(int remLev, DateTime day){
        int dayOffset = 0;
        switch(remLev){
            case 0: dayOffset = 1; break;//初学習のときのみ
            case 1: dayOffset = 1; break;
            case 2: dayOffset = 3; break;
            case 3: dayOffset = 7; break;
            case 4: dayOffset = 14; break;
            case 5: dayOffset = 30; break;
            case 6: dayOffset = 90; break;
            default: dayOffset = 180; break;
        }
        return day +  new TimeSpan(dayOffset, 0, 0, 0);
    }

    public List<Record> PickUpNextAppearanceRecord(DateTime day, bool doesCheckBeforeDays = false)
    {
        if(doesCheckBeforeDays){
            return RecordList
                .Where(rec => rec.NextAppearanceDay <= day)
                .ToList();
        }

        return RecordList
               .Where(rec => rec.NextAppearanceDay == day)
               .ToList();
    }
    public List<Record> PickUpLastMarkedRecord(DateTime day, bool doesCheckBeforeDays = false)
    {
        if(doesCheckBeforeDays){
            return RecordList
                .Where(rec => rec.LastMarkedDay <= day)
                .ToList();
        }

        return RecordList
               .Where(rec => rec.LastMarkedDay == day)
               .ToList();
    }
}

//TODO: あとでコンフィグファイルにする
public class ConfigManager
{
    public string RecordFilePath{ get; private set; }
    public DateTime LastExecutionDay{ get; private set; }
    public string ReviewOffsetsStr{ get; private set; }
    public DateTime ManipulatingDay{ get; set;}

    public ConfigManager()
    {
        LoadConfigFromFile();
    }

    public void LoadConfigFromFile()
    {
        try{
            using(StreamReader sr = new StreamReader(
        (System.IO.Stream)File.OpenRead("tempconfig.text"), Encoding.GetEncoding("UTF-8"))
            ){
                RecordFilePath = sr.ReadLine();
                LastExecutionDay = DateTime.Parse(sr.ReadLine());
                ReviewOffsetsStr = sr.ReadLine();
                ManipulatingDay = DateTime.Today;
            }
        }catch(Exception e){
            Console.WriteLine(e.Message);
        }
    }

    public void SaveConfigToFile()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(RecordFilePath);
        sb.AppendLine();
        sb.Append(LastExecutionDay);
        sb.AppendLine();
        File.WriteAllText("tempconfig.text", sb.ToString(), System.Text.Encoding.GetEncoding("UTF-8"));
    }
}

//TODO: コンストラクタについて、引数がなくても作れるようにする？
public class Record
{
    public int TaskID { get; set; }
    public DateTime CreatedDay { get; set; }
    public DateTime NextAppearanceDay { get; set; }
    public DateTime LastMarkedDay { get; set; }
    public int LastRemembranceLevel {get; set; }
    public int RemembranceLevel { get; set; }
    public int CumulativeMistakes { get; set; }
    public string Content { get; set; }

    public Record(string content, int taskID, DateTime day)
    {
        TaskID = taskID;
        CreatedDay = day;
        NextAppearanceDay = day;
        LastMarkedDay = day - new TimeSpan(1,0,0,0);
        LastRemembranceLevel = 0;
        RemembranceLevel = 0;
        CumulativeMistakes = 0;
        Content = content;
    }

    public bool SetRecord(string taskID, string createdDay, string nextAppearanceDay, string lastMarkedDay, string lastRemembranceLevel, string remembranceLevel, string cumulativeMistakes, string content)
    {
        DateTime cDay, nDay, lDay;
        int tID, lLevel, rLevel, cMistakes;

        if(    !int.TryParse(taskID, out tID)
            || !DateTime.TryParse(createdDay, out cDay)
            || !DateTime.TryParse(nextAppearanceDay, out nDay)
            || !DateTime.TryParse(lastMarkedDay, out lDay)
            || !int.TryParse(lastRemembranceLevel, out lLevel)
            || !int.TryParse(remembranceLevel, out rLevel)
            || !int.TryParse(cumulativeMistakes, out cMistakes)
            || content.Length > 100
        ) return false;
        
        TaskID = tID;
        CreatedDay = cDay;
        NextAppearanceDay = nDay;
        LastMarkedDay = lDay;
        LastRemembranceLevel = lLevel;
        RemembranceLevel = rLevel;
        CumulativeMistakes = cMistakes;
        Content = content;
        return true;
    }

    public string CSV()
    {
        StringBuilder sb = new StringBuilder();
        PropertyInfo[] infoArray = GetType().GetProperties();
        foreach (PropertyInfo info in infoArray)
        {
            var value = info.GetValue(this,null);
            if(value.GetType() == typeof(DateTime)){
                sb.Append(DateTime.Parse(value.ToString()).ToString("MM/dd/yyyy"));
            }else{
                sb.Append(value);
            }
            sb.Append(",");
        }
        sb.Length -= 1;
       
        return sb.ToString();
    }
}